import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Code to break the cipher text encrypted with a Vigenere cipher
 * @author Ryan Gerald Conley - rgc11
 *
 */
public class CodeBreaker {
	public static void main(String[] args) {
		int leastMultiple = Integer.MAX_VALUE;
		String line = null;
		String firstLine = null;
		String cipherText = "";
		
		// read in hw2-vignere.txt
		try {
			BufferedReader br = new BufferedReader(new FileReader(
					new File("hw2-vigenere.txt")));
			HashMap<String, Integer> wordIndex = new HashMap<String, Integer>();
			int charRead = 0;
			while((line = br.readLine()) != null) {
				cipherText += line.replaceAll("[^a-z\\s]", "");
				String[] words = line.replaceAll("[^a-z\\s]", "").split(" ");
				if(firstLine == null) {
					firstLine = line.replaceAll("[^a-z\\s]", "");
				}
				for(int i = 0; i < words.length; i++) {
					if(wordIndex.containsKey(words[i])) {
						// save the distances between identically encrypted words
						if((charRead - wordIndex.get(words[i])) < leastMultiple) {
							leastMultiple = charRead - wordIndex.get(words[i]);
						}
					}
					wordIndex.put(words[i], charRead);
					charRead += words[i].length();
				}
			}
			br.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		// calculate possible multiples
		// 1 < multiple <= smallest found distance
		ArrayList<Integer> multiples = new ArrayList<Integer>();
		multiples.add(leastMultiple);
		for(int i = 2; i < Math.sqrt((double)leastMultiple); i++) {
			if(leastMultiple % i == 0) {
				multiples.add(i);
				multiples.add(leastMultiple/i);
			}
		}

		Collections.sort(multiples);

		// create english dictionary to verify decrypted message
		HashSet<String> dictionary = new HashSet<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("en_US.dic"));
			String dicLine = null;
			while((dicLine = br.readLine()) != null) {
				if(dicLine.contains("/")) {
					int index = dicLine.indexOf('/');
					dicLine = dicLine.substring(0, index);
				}
				dictionary.add(dicLine);
			}
			br.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		// generate random strings with lower-case alphabetical characters knowing "so"
		// is a part of the key due to the ciphertext "ug1653"
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String[] alpha = alphabet.split("");
		String vKey = null;

		keySearch:
		for(Integer keyLength : multiples) {
			// check all keys in the given keyspace, using the first line of ciphertext to verify
			for(int i = keyLength-2; i>= 0; i--) {
				String[] key = new String[keyLength-1];
				key[i] = "so";
				vKey = checkKeys(key, keyLength, alpha, firstLine, dictionary);
				if(vKey != null) break keySearch;
			}
		}
		
		System.out.println("Key found: " + vKey + ". Decrypting text...");
		
		String decryptedText = decrypt(cipherText, vKey, alpha);
		System.out.println(decryptedText);
		
	}
	
	public static String checkKeys(String[] key, int length, 
			String[] alphabet, String cipherText, HashSet<String> dictionary) {
		if(alToString(key).length() == length) {
			// decrypt and check
			System.out.println(alToString(key).toString());
			if(isProbableKey(decrypt(cipherText, alToString(key), alphabet), dictionary)) {
				return alToString(key);
			}
			
			return null;
		}
		
		for(String letter : alphabet) {
			for(int i = 0; i < key.length; i++) {
				if(key[i] == null) {
					key[i] = letter;
					break;
				}
			}
			if(checkKeys(key, length, alphabet, cipherText, dictionary) != null) {
				return alToString(key);
			}
			for(int i = key.length-1; i >= 0; i--) {
				if(key[i] == null || key[i].equals("so")) continue;
				else {
					key[i] = null;
					break;
				}
			}
		}
		
		return null;
	}
	
	public static String decrypt(String cipherText, String key, String[] alpha) {
		String decrypted = "";
		Pattern p = Pattern.compile("[a-z]");
		Matcher m = null;
		int count = 0;
		for(int i = 0; i < cipherText.length(); i++) {
			if(p.matcher(cipherText.charAt(i) + "").matches()) {
				int dVal = ((int)cipherText.charAt(i) - 
						((int)key.charAt(count%key.length()) - (int)'a'));
				if(dVal - (int)'a' < 0) {
					dVal = (int)'z' + 1 - ((int)'a' - dVal);
				}
				decrypted += (char)dVal;
				count++;
			}
			else {
				decrypted += cipherText.charAt(i);
			}
		}
		
		return decrypted;
	}
	
	public static boolean isProbableKey(String decryptedText, HashSet<String> dictionary) {
		decryptedText= decryptedText.replaceAll("[^a-z\\s]", "");
		String words[] = decryptedText.split(" ");
		int count = 0;
		for(String word : words) {
			if(dictionary.contains(word)) {
				count++;
			}
		}
		if(((double)count/(double)words.length) >= 0.75) {
			return true;
		}
		return false;
	}
	
	public static String alToString(String[] al) {
		String str = "";
		for(String word : al) {
			str += word;
		}
		return str;
	}
}
